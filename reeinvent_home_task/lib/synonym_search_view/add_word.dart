import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:reeinvent_home_task/helper/global.dart' as globals;
import 'package:reeinvent_home_task/helper/text_styles.dart';
import 'package:reeinvent_home_task/model/synonymsModel.dart';
import 'package:reeinvent_home_task/model/wordsModel.dart';
import 'package:toast/toast.dart';

class AddWord extends StatefulWidget {
  final WordsModel model;

  const AddWord({Key key, this.model}) : super(key: key);

  @override
  _AddWordState createState() => _AddWordState(this.model);
}

class _AddWordState extends State<AddWord> {
  final WordsModel model;
  int isSelected=-1;

  Size size;
  final TextEditingController _searchEditTextController =
      TextEditingController();

  _AddWordState(this.model);

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: const Text("Add New Word"),
        centerTitle: true,
      ),
      body: Container(
        color: globals.backgroundColor,
        child: Column(
          children: [
            ///  add synonym container
            Container(
                width: size.width,
                margin: EdgeInsets.all(20),
                child: TextFormField(
                  textInputAction: TextInputAction.done,
                  enabled: true,
                  textCapitalization: TextCapitalization.sentences,
                  decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: globals.kPrimaryColor, width: 1.0),
                      ),
                      fillColor: globals.kPrimaryColor,
                      border: OutlineInputBorder(),
                      hintStyle: TextStyle(color: globals.kPrimaryColor),
                      labelText: "Add word"),
                  controller: _searchEditTextController,
                  style: TextStyles.getNoteTextStyle(globals.kPrimaryColor),
                  keyboardType: TextInputType.text,
                  inputFormatters: <TextInputFormatter>[
                    FilteringTextInputFormatter.allow(RegExp(r'[a-z A-Z]')),
                  ],

                )),

            MaterialButton(
              color: globals.kPrimaryColor,
              onPressed: () {
                addNewWord();
              },
              child: Text(
                "Add",
                style: TextStyle(color: globals.backgroundColor),
              ),
            ),

            /// Synonyms list
            Expanded(
                child: Container(
                    child: ListView.builder(
              itemCount: model.synonymsWordList.length,
              itemBuilder: (context, index) {
                return InkWell(
                  child: Container(
                    color: isSelected==index?globals.kPrimaryColor:Colors.transparent,
                      child: Column(
                    children: [
                      ListTile(
                        title: Text(
                          model.synonymsWordList.elementAt(index).getWord,
                          style: TextStyles.getNoteTextStyle(
                              isSelected==index?globals.backgroundColor:globals.kPrimaryColor),
                        ),
                      ),
                      Divider(
                        thickness: 1,
                        color: isSelected==index?globals.backgroundColor:globals.blackColor,
                      )
                    ],
                  )),
                  onTap: () {
                    isSelected = index;
                    setState(() {});
                  },
                );
              },
            )))
          ],
        ),
      ),
    );
  }



  void addNewWord()async{
    if(isSelected>=0){
      await model.addWordWithPreviousSelection(isSelected,_searchEditTextController.text.trim());
    }
    else{
     await model.addWordWithoutPreviousSelection(_searchEditTextController.text.trim());
    }
    Toast.show(globals.successfullyAdded, context,);
    Navigator.pop(context);
  }
}
