import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:reeinvent_home_task/helper/global.dart' as globals;
import 'package:reeinvent_home_task/helper/navigation_helper.dart';
import 'package:reeinvent_home_task/helper/text_styles.dart';
import 'package:reeinvent_home_task/model/wordsModel.dart';

class SynonymsSearchView extends StatefulWidget {
  final WordsModel model;

  const SynonymsSearchView({Key key, this.model}) : super(key: key);

  @override
  _SynonymsSearchViewState createState() =>
      _SynonymsSearchViewState(this.model);
}

class _SynonymsSearchViewState extends State<SynonymsSearchView> {
  final WordsModel model;

  Size size;
  final TextEditingController _searchEditTextController =
      TextEditingController();

  _SynonymsSearchViewState(this.model);

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: const Text("Reinvent home task"),
        centerTitle: true,
        actions: [
          IconButton(
            padding: EdgeInsets.symmetric(vertical: 10),
            icon: Icon(Icons.add,color: globals.backgroundColor,size: 40,),
            onPressed: () {
              NavigationManager.navigateToSynonymsAddView(context, model);
            },
          ),
          // add more IconButton
        ],
      ),
      body: Container(
        color: globals.backgroundColor,
        child: Column(
          children: [
            /// search container
            Container(
                width: size.width,
                margin: EdgeInsets.all(20),
                child: TextFormField(
                  textInputAction: TextInputAction.done,
                  enabled: true,
                  textCapitalization: TextCapitalization.sentences,
                  decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: globals.kPrimaryColor, width: 1.0),
                      ),
                      fillColor: globals.kPrimaryColor,
                      border: OutlineInputBorder(),
                      hintStyle: TextStyle(color: globals.kPrimaryColor),
                      labelText: "Search synonyms"),
                  controller: _searchEditTextController,
                  style: TextStyles.getNoteTextStyle(globals.kPrimaryColor),
                  keyboardType: TextInputType.text,
                  inputFormatters: <TextInputFormatter>[
                    FilteringTextInputFormatter.allow(RegExp(r'[a-z A-Z]')),
                  ],
                  onChanged: (text) {
                    print(text);
                    if (text.toString().isEmpty) {
                      model.synonymsList.clear();
                      setState(() {});
                    } else {
                      getSynonymsList(text.trim());
                    }
                  },
                )),

            /// Synonyms list
            Expanded(
                child: Container(
                    child: model.synonymsList.length > 0
                        ? ListView.builder(
                            itemCount: model.synonymsList.length,
                            itemBuilder: (context, index) {
                              return Slidable(
                                actionPane: SlidableDrawerActionPane(),
                                actionExtentRatio: 0.25,
                                child: Column(
                                  children: [
                                    ListTile(
                                      title: Text(
                                        model.synonymsList.elementAt(index),
                                        style: TextStyles.getNoteTextStyle(
                                            globals.kPrimaryColor),
                                      ),
                                    ),
                                    Divider(
                                      thickness: 1,
                                      color: globals.blackColor,
                                    )
                                  ],
                                ),
                                actions: <Widget>[
                                  IconSlideAction(
                                    caption: 'Delete',
                                    color: Colors.red,
                                    icon: Icons.delete,
                                    onTap: () {
                                      deleteWord(
                                          model.synonymsList.elementAt(index));
                                    },
                                  ),
                                ],
                                secondaryActions: <Widget>[
                                  IconSlideAction(
                                    caption: 'Delete',
                                    color: Colors.red,
                                    icon: Icons.delete,
                                    onTap: () {
                                      deleteWord(
                                          model.synonymsList.elementAt(index));
                                    },
                                  ),
                                ],
                              );
                            },
                          )
                        : Center(
                            child: Text("No Word Found!",
                                style: TextStyles.getTitleStyle(
                                    globals.kPrimaryColor)))))
          ],
        ),
      ),
    );
  }

  void getSynonymsList(String word) async {
    await model.getSynonymsList(word);
    setState(() {});
  }

  void deleteWord(String word) async{
    await model.deleteWord(word);
    setState(() {});
  }
}
