import 'dart:async';

import 'package:flutter/material.dart';
import 'package:reeinvent_home_task/helper/navigation_helper.dart';
import 'package:reeinvent_home_task/helper/text_styles.dart';
import 'package:reeinvent_home_task/model/wordsModel.dart';
import 'package:reeinvent_home_task/widgets/app_error_widget.dart';
import 'package:reeinvent_home_task/helper/global.dart' as globals;
import 'package:scoped_model/scoped_model.dart';

void main() {
  /// Error widget if our app is crashed so this layout will show
  /// instead of that red screen
  ErrorWidget.builder = (errorDetails) {
    return AppErrorWidget(
      errorDetails: errorDetails,
    );
  };

  runApp(MyApp());
}

class MyApp extends StatelessWidget {




  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Flutter Work Task',
          theme: ThemeData(
            primarySwatch: globals.kPrimaryColor,
          ),
          home: Splash(),
        );
  }
}

class Splash extends StatefulWidget {

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<Splash> {
  WordsModel model;
  List<String> synonyms1=["large","broad","huge","full","giant","grand","vast","wide","sizable","generous","big"];
  List<String> synonyms2=["part","any","chunk","sector","detail","item","piece","unit","side","section","member"];
  @override
  void initState() {
    super.initState();
    model=WordsModel();
    splashDelayed();
  }

  /// splash timer and navigate to synonyms search view class
  void splashDelayed() async {
   // Timer(Duration(seconds: 1), navigateToCalendarView);
    await model.setDataBase(synonyms1);
    await model.setDataBase(synonyms2);
    navigateToCalendarView();

  }


  @override
  Widget build(BuildContext context) {
    return ScopedModel<WordsModel>(
        model: model,
        child:Scaffold(
      backgroundColor: globals.themColor,
      body: Center(
        child: Text(
          'Home Work Task',
          style: TextStyles.getTitleStyle(globals.backgroundColor),
        ),
      ),
    ));
  }

  /// navigate to calender view
  void navigateToCalendarView() {
    print(model.synonymsWordList.length);
    NavigationManager.navigateToSynonymsSearchView(context,model);
  }
}
