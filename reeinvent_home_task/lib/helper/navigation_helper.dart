import 'package:flutter/material.dart';
import 'package:reeinvent_home_task/model/wordsModel.dart';
import 'package:reeinvent_home_task/synonym_search_view/add_word.dart';
import 'package:reeinvent_home_task/synonym_search_view/synonym_search_view.dart';

/// Class to manage navigation across the app from one screen to another.
///
/// This class include several methods which are redirecting users
/// to another screen.
class NavigationManager {

  /// Navigate to Synonyms List View
  /// Parameters : [BuildContext]
  static void navigateToSynonymsSearchView(BuildContext context,WordsModel model) {
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => SynonymsSearchView(model: model)),
        (route) => false);
  }


  /// Navigate to Synonyms Add View
  /// Parameters : [BuildContext]
  static void navigateToSynonymsAddView(BuildContext context,WordsModel model) {
    Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => AddWord(model: model)),
            );
  }

}
