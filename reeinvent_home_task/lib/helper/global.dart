import 'dart:ui';

import 'package:flutter/material.dart';

/// we will keep our static String and static colors
/// like Base Url's and other static Strings



/// error message
String errorTitle="Error";
String errorMessage="There is some thing wrong with the application. Please, restart the application";

String successfullyAdded="Word successfully added!";


/// colors

const themColor = Color(0xff5743be);

const dividerColor = Color(0xff454545);

const weekListBackgroundColor = Color(0xffeeecf9);

const backgroundColor = Color(0xffffffff);

const blackColor = Color(0xff000000);

const inActiveDaysColor= Color(0xffb3b4b8);

const activeDaysColor= Color(0xffb3b4b8);

const lowReviewColor = Color(0xfff59542);

const highReviewColor = Color(0xff56db21);

/// Material

MaterialColor kPrimaryColor = const MaterialColor(
  0xff5743be,
  const <int, Color>{
    50: const Color(0xff5743be),
    100: const Color(0xff5743be),
    200: const Color(0xff5743be),
    300: const Color(0xff5743be),
    400: const Color(0xff5743be),
    500: const Color(0xff5743be),
    600: const Color(0xff5743be),
    700: const Color(0xff5743be),
    800: const Color(0xff5743be),
    900: const Color(0xff5743be),
  },
);