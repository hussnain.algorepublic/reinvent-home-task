import 'package:reeinvent_home_task/model/synonymsModel.dart';
import 'package:scoped_model/scoped_model.dart';

class WordsModel extends Model {
  List<Synonyms> synonymsWordList=[];
  List<String> synonymsList=[];


  Future<void> setDataBase(List<String> list) async{
    for(int i=0; i<list.length;i++){
      Synonyms synonyms=new Synonyms();
      synonyms.setWord(list.elementAt(i).toString());
      synonyms.setListOfSynonyms(i, list);
      synonymsWordList.add(synonyms);
    }
  }

  Future<void> getSynonymsList(String word)async{
    synonymsList.clear();
    for(int i=0;i<synonymsWordList.length;i++){
     if(synonymsWordList.elementAt(i).getWord.toString().toLowerCase().contains(word.toString().toLowerCase())){
       synonymsList.addAll(synonymsWordList.elementAt(i).getListOfSynonyms);
     }
    }
  }

  Future<void> deleteWord(String word)async{
    synonymsList.remove(word);
   int index= synonymsWordList.indexWhere((element) => element.getWord==word);
   if(index>0){
   synonymsWordList.removeAt(index);}
   synonymsWordList.forEach((element) {
     element.getListOfSynonyms.remove(word);
   });
  }

  Future<void> addWordWithoutPreviousSelection(String word)async{
    Synonyms synonyms=new Synonyms();
    synonyms.setWord(word);
    synonymsWordList.add(synonyms);
  }

  Future<void> addWordWithPreviousSelection(int index,String word)async{
    List<String> listOfSynonyms=[];
    Synonyms synonyms=new Synonyms();
    synonyms.setWord(word);
    listOfSynonyms.addAll(synonymsWordList.elementAt(index).getListOfSynonyms);
    listOfSynonyms.add(synonymsWordList.elementAt(index).getWord);
    synonyms.getListOfSynonyms.addAll(listOfSynonyms);
    synonyms.getListOfSynonyms.forEach((element1) {
      print(element1);
      int index=synonymsWordList.indexWhere((element2) => element2.getWord.toString().toLowerCase()==element1.toString().toLowerCase());
      if(index>0){
        print(index);

        synonymsWordList.elementAt(index).getListOfSynonyms.add(word);
       print(synonymsWordList.elementAt(index).getListOfSynonyms.length);
      }
    });
    print(listOfSynonyms);
    synonymsWordList.add(synonyms);
  }


}