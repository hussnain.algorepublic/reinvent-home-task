

class Synonyms {

  String _word;

  List<String> listOfSynonyms=[];


  void setWord(String value) {
    _word = value;
  }

  String get getWord => _word ?? "";

  void setListOfSynonyms(int index,List<String> value) {
    for(int i =0; i< value.length;i++){
      if(i!=index){
      this.listOfSynonyms.add(value.elementAt(i).toString());
      }
    }
  }

  List<String> get getListOfSynonyms => listOfSynonyms ?? [];


}